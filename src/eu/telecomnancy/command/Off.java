package eu.telecomnancy.command;

import eu.telecomnancy.proxy.ProxySensor;

/* COMMAND PATTER : This class creates Off Command objects that turn off a sensor. */

public class Off implements Command {

	private ProxySensor sensor;
	
	public Off(ProxySensor sensor) {
		this.sensor = sensor;
	}
	
	@Override
	public void execute() {
		sensor.off();
	}

}
