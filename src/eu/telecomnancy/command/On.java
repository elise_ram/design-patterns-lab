package eu.telecomnancy.command;

import eu.telecomnancy.proxy.ProxySensor;

/* COMMAND PATTERN : This class creates On Command objects that turn on a sensor. */

public class On implements Command {

	private ProxySensor sensor;
	
	public On(ProxySensor sensor) {
		this.sensor = sensor;
	}
	
	@Override
	public void execute() {
		sensor.on();	
	}

}
