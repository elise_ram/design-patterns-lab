package eu.telecomnancy.command;

import eu.telecomnancy.proxy.ProxySensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

/* COMMAND PATTERN : This class creates Update Command object that update a sensor. */

public class Update implements Command {

	private ProxySensor sensor;
	
	public Update(ProxySensor sensor) {
		this.sensor = sensor;
	}
	
	@Override
	public void execute() throws SensorNotActivatedException {
		sensor.update();
	}

}
