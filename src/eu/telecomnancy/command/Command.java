package eu.telecomnancy.command;

import eu.telecomnancy.sensor.SensorNotActivatedException;

/* COMMAND PATTERN : This class must be implemented by the Command class. */

public interface Command {

	public void execute() throws SensorNotActivatedException;
	
}
