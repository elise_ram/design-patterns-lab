package eu.telecomnancy.sensor;

import java.util.Random;

import eu.telecomnancy.ui.Observer;

/* STATE PATTERN : One state of a PersonalSensor object. */

public class TurnedOn implements PersonalSensorState {

	private PersonalSensor sensor;
	
	public TurnedOn(PersonalSensor sensor) {
		this.sensor = sensor;
	}
	
	@Override
	public void on() { }

	@Override
	public void off() {
		sensor.setState(sensor.getTurnedOffState());
		sensor.notifyObserver();
	}

	@Override
	public void update() {
		sensor.value = (new Random()).nextDouble() * 100;
        System.out.println("valeur : "+ sensor.value);
        sensor.notifyObserver();
	}

	@Override
	public boolean getStatus() {
		return true;
	}

	@Override
	public double getValue() {
		return sensor.value;
	}

	@Override
	public void register(Observer observer) {
		sensor.observers.add(observer);	
	}

	@Override
	public void unregister(Observer observer) {
		int observerIndex = sensor.observers.indexOf(observer);
		sensor.observers.remove(observerIndex);
	}

	@Override
	public void notifyObserver() {
		for(Observer observer : sensor.observers) observer.update();	
	}

}
