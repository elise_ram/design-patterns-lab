package eu.telecomnancy.sensor;

import java.util.concurrent.CopyOnWriteArrayList;

import eu.telecomnancy.ui.Observer;

/* STATE PATTERN : Allow an object to alter its behavior when its internal state changes. */

public class PersonalSensor implements ISensor {

	
	private PersonalSensorState turnedOn = new TurnedOn(this);
	private PersonalSensorState turnedOff = new TurnedOff(this);
	private PersonalSensorState state = turnedOff;
	CopyOnWriteArrayList<Observer> observers = new CopyOnWriteArrayList<Observer>();
	boolean unitFahrenheit = false;
	double value = 0;
    
    public void setState(PersonalSensorState state) {
    	this.state = state;
    }

	@Override
	public void on() {
		state.on();
	}

	@Override
	public void off() {
		state.off();
	}

	@Override
	public boolean getStatus() {
		return state.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		state.update();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return state.getValue();
	}

	public PersonalSensorState getTurnedOnState() {
		return turnedOn;
	}
	
	public PersonalSensorState getTurnedOffState() {
		return turnedOff;
	}
	
	@Override
	public void register(Observer observer) {
		state.register(observer);
	}

	@Override
	public void unregister(Observer observer) {
		state.unregister(observer);
	}

	@Override
	public void notifyObserver() {
		state.notifyObserver();
	}
	
	@Override
	public void setUnitFahrenheit(boolean value) {
		unitFahrenheit = value;		
	}
	
	@Override
	public boolean getUnitFahrenheit() {
		return unitFahrenheit;
	}
	
}

