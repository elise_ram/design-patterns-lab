package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.Observer;

/* STATE PATTERN : Interface implemented by the different states of a PersonalSensor. */

public interface PersonalSensorState {
	
	public void on();
    public void off();
    public boolean getStatus();
    public void update() throws SensorNotActivatedException;
    public double getValue() throws SensorNotActivatedException;
    public void register(Observer observer);
	public void unregister(Observer observer);
	public void notifyObserver();
	
}
