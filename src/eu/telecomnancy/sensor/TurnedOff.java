package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.Observer;

/* STATE PATTERN : One state of a PersonalSensor object. */

public class TurnedOff implements PersonalSensorState {

	private PersonalSensor sensor;
	
	public TurnedOff(PersonalSensor sensor) {
		this.sensor = sensor;
	}
	
	@Override
	public void on() {
		sensor.setState(sensor.getTurnedOnState());
	}

	@Override
	public void off() { }

	@Override
	public void update() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}

	@Override
	public boolean getStatus() {
		return false;
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}

	@Override
	public void register(Observer observer) {
		sensor.observers.add(observer);	
	}

	@Override
	public void unregister(Observer observer) {
		int observerIndex = sensor.observers.indexOf(observer);
		sensor.observers.remove(observerIndex);
	}

	@Override
	public void notifyObserver() {
		for(Observer observer : sensor.observers) observer.update();	
	}

}
