package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.Observer;

/* OBSERVER PATTERN : Interface for attaching and detaching Observer objects. */

public interface Subject {

	// Create a new Observer object.
	public void register(Observer observer);
	
	// Delete an Observer object.
	public void unregister(Observer observer);
	
	// Notify Observer objects they should be updated.
	public void notifyObserver() throws SensorNotActivatedException;


}
