package eu.telecomnancy.sensor;

import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

import eu.telecomnancy.ui.Observer;


public class TemperatureSensor implements ISensor {
    private boolean state;
    private double value = 0;
    private boolean unitFahrenheit = false;
    private CopyOnWriteArrayList<Observer> observers = new CopyOnWriteArrayList<Observer>();

    @Override
    public void on() {
        state = true;
    }

    @Override
    public void off() {
        state = false;
        notifyObserver();
    }

    @Override
    public boolean getStatus() {
        return state;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        if (state) {
            value = (new Random()).nextDouble() * 100;
            System.out.println("valeur : "+ value);
            notifyObserver();
        }
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (state) 
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }
    
	@Override
	public void register(Observer observer) {
		observers.add(observer);
	}

	@Override
	public void unregister(Observer observer) {
		int observerIndex = observers.indexOf(observer);
		observers.remove(observerIndex);
	}

	@Override
	public void notifyObserver() {
		for(Observer observer : observers) observer.update();
	}
	
	@Override
	public void setUnitFahrenheit(boolean value) {
		unitFahrenheit = value;		
	}

	@Override
	public boolean getUnitFahrenheit() {
		return unitFahrenheit;
	}

}
