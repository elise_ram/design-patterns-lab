package eu.telecomnancy.sensor;

import java.util.concurrent.CopyOnWriteArrayList;

import eu.telecomnancy.ui.Observer;

/* ADAPTER PATTERN : Convert the class LegacyTemperatureSensor into the ISensor interface expect. */

public class LegacyTemperatureSensorAdapter implements ISensor {
	
	private LegacyTemperatureSensor oldSensor;
	private CopyOnWriteArrayList<Observer> observers = new CopyOnWriteArrayList<Observer>();
	
	private boolean unitFahrenheit = false;
	
	// Constructors
	public LegacyTemperatureSensorAdapter() {
		oldSensor = new LegacyTemperatureSensor();
	}
	
	public LegacyTemperatureSensorAdapter(LegacyTemperatureSensor sensor) {
		oldSensor = sensor;
	}

	@Override
	public void on() {
		if(!oldSensor.getStatus()) {
			oldSensor.onOff();
		}
		
	}

	@Override
	public void off() {
		if(oldSensor.getStatus()) {
			oldSensor.onOff();
			notifyObserver();
		}
	}

	@Override
	public boolean getStatus() {
		return oldSensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		// Update a LegacyTemperatureSensor is like reset the sensor by onOff twice.
		if(oldSensor.getStatus()) {
			oldSensor.onOff();
			oldSensor.onOff();
			System.out.println("valeur : "+ oldSensor.getTemperature());
			notifyObserver();
		}
		else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		if(oldSensor.getStatus()) return oldSensor.getTemperature();
		else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}

	@Override
	public void register(Observer observer) {
		observers.add(observer);
	}

	@Override
	public void unregister(Observer observer) {
		int observerIndex = observers.indexOf(observer);
		observers.remove(observerIndex);
	}

	@Override
	public void notifyObserver() {
		for(Observer observer : observers) observer.update();
	}

	@Override
	public void setUnitFahrenheit(boolean value) {
		unitFahrenheit = value;		
	}

	@Override
	public boolean getUnitFahrenheit() {
		return unitFahrenheit;
	}
	
}
