package eu.telecomnancy.ui;

import eu.telecomnancy.command.Off;
import eu.telecomnancy.command.On;
import eu.telecomnancy.command.Update;
import eu.telecomnancy.proxy.ProxySensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SensorView extends JPanel implements Observer {
    private ProxySensor sensor;

    private JLabel value;
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    
    private On onCommand;
    private Off offCommand;
    private Update updateCommand;
    
    
    public SensorView(ProxySensor c) {
        this.sensor = c;
        
        onCommand = new On(sensor);
        offCommand = new Off(sensor);
        updateCommand = new Update(sensor);
        
        
        c.register(this);
        
        if(sensor.getUnitFahrenheit()) value = new JLabel("N/A °F");
        else value = new JLabel("N/A °C");
       
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onCommand.execute();
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                offCommand.execute();
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    updateCommand.execute();
                } catch (SensorNotActivatedException sensorNotActivatedException) {
                    sensorNotActivatedException.printStackTrace();
                }
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }
    
	@Override
	public void update() {	
		double value;
		try {
			value = sensor.getValue();
			if(sensor.getUnitFahrenheit()) this.value.setText(value + "°F");
			else this.value.setText(value + "°C");
		}
		catch(SensorNotActivatedException e) {
			if(sensor.getUnitFahrenheit()) this.value.setText("N/A °F");
			else this.value.setText("N/A °C");
		}
		   
	}
	
}
