package eu.telecomnancy.ui;


/* OBSERVER PATTERN : Updating interface for objects that should be notified of changes in a Subject object. */

public interface Observer {

	// Update an Observer object with an updated Subject sensor.
	public void update();
	
}
