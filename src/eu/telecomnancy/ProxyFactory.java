package eu.telecomnancy;

import eu.telecomnancy.decorators.*;
import eu.telecomnancy.proxy.*;

/* FACTORY PATTERN : This class can create all types of sensor possible. */

public class ProxyFactory {

	public ProxySensor makeProxySensor (String sensorType) {
		
		ProxySensor sensor;
		
		if(sensorType.equals("LTS")) {
			sensor = new LegacyTemperatureSensorProxy();
		}
		else if(sensorType.equals("PS")) {
			sensor = new PersonalSensorProxy();
		}
		else if(sensorType.equals("TS")) {
			sensor = new TemperatureSensorProxy();
		}
		else if(sensorType.equals("FLTS")) {
			sensor = new FahrenheitUnit(new LegacyTemperatureSensorProxy());
		}
		else if(sensorType.equals("FPS")) {
			sensor = new FahrenheitUnit(new PersonalSensorProxy());
		}
		else if(sensorType.equals("FTS")) {
			sensor = new FahrenheitUnit(new TemperatureSensorProxy());
		}
		else if(sensorType.equals("RLTS")) {
			sensor = new RoundedValue(new LegacyTemperatureSensorProxy());
		}
		else if(sensorType.equals("RPS")) {
			sensor = new RoundedValue(new PersonalSensorProxy());
		}
		else if(sensorType.equals("RTS")) {
			sensor = new RoundedValue(new TemperatureSensorProxy());
		}
		else if(sensorType.equals("RFLTS")) {
			sensor = new RoundedValue(new FahrenheitUnit(new LegacyTemperatureSensorProxy()));
		}
		else if(sensorType.equals("RFPS")) {
			sensor = new RoundedValue(new FahrenheitUnit(new PersonalSensorProxy()));
		}
		else if(sensorType.equals("RFTS")) {
			sensor = new RoundedValue(new FahrenheitUnit(new TemperatureSensorProxy()));
		}
		else sensor = new TemperatureSensorProxy();
		
		return sensor;
		
	}
}
