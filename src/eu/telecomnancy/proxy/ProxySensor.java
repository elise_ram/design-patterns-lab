package eu.telecomnancy.proxy;

import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.ui.Observer;

/* PROXY PATTERN : Interface implemented by the proxy objects. */

public interface ProxySensor {

	public void on();
	public void off();
	public boolean getStatus();
	public void update() throws SensorNotActivatedException;
	public double getValue() throws SensorNotActivatedException;
	public void register(Observer observer);
	public void setUnitFahrenheit(boolean value);
	public boolean getUnitFahrenheit();
	
}
