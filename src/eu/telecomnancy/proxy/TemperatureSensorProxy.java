package eu.telecomnancy.proxy;

import java.sql.Date;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.Observer;
import eu.telecomnancy.sensor.SensorNotActivatedException;

/* PROXY PATTERN : Proxy for a TemperatureSensor object. */

public class TemperatureSensorProxy implements ProxySensor {

	private ISensor realSensor = new TemperatureSensor();
	private Date date; 
	
	@Override
	public void on() {
		realSensor.on();
		date = new java.sql.Date(System.currentTimeMillis());
		System.out.println("Date : " + date + " - " + "Command : on - Return value : void");
	}

	@Override
	public void off() {
		realSensor.off();
		date = new java.sql.Date(System.currentTimeMillis());
		System.out.println("Date : " + date + " - " + "Command : off - Return value : void");
		
	}

	@Override
	public boolean getStatus() {
		boolean status;
		status = realSensor.getStatus();
		date = new java.sql.Date(System.currentTimeMillis());
		System.out.println("Date : " + date + " - " + "Command : getStatus - Return value : " + status);
		return status;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		realSensor.update();
		date = new java.sql.Date(System.currentTimeMillis());
		System.out.println("Date : " + date + " - " + "Command : update - Return value : void");	
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		double value;
		value = realSensor.getValue();
		date = new java.sql.Date(System.currentTimeMillis());
		System.out.println("Date : " + date + " - " + "Command : getValue - Return value : " + value);
		return value;
	}

	@Override
	public void register(Observer observer) {
		realSensor.register(observer);	
	}

	@Override
	public void setUnitFahrenheit(boolean value) {
		realSensor.setUnitFahrenheit(value);
	}
	
	@Override
	public boolean getUnitFahrenheit() {
		return realSensor.getUnitFahrenheit();
	}
	
}
