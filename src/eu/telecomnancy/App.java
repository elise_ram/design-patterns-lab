package eu.telecomnancy;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import eu.telecomnancy.proxy.ProxySensor;
import eu.telecomnancy.ui.ConsoleUI;

public class App {

    public static void main(String[] args) {
    	Properties prop = new Properties();
    	FileInputStream in = null;
		try {
			in = new FileInputStream("src/eu/telecomnancy/app.properties");
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
    	try {
			prop.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
    	try {
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	String typeSensor = prop.getProperty("LegacyTemperatureSensorFahrenheitRounded");
    	ProxyFactory factory = new ProxyFactory();
    	ProxySensor sensor = factory.makeProxySensor(typeSensor);
        new ConsoleUI(sensor);
    }

}