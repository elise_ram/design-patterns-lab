package eu.telecomnancy.decorators;

import eu.telecomnancy.proxy.ProxySensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.ui.Observer;

/* DECORATOR PATTERN : Abstract class that all the decorators inherite. */

public abstract class ValueDecorator implements ProxySensor {

	protected ProxySensor sensor;
	
	public ValueDecorator (ProxySensor sensor) {
		this.sensor = sensor;
	}
	
	@Override
	public void register(Observer observer) {
		sensor.register(observer);
	}

	@Override
	public void on() {
		sensor.on();
	}

	@Override
	public void off() {
		sensor.off();
	}

	@Override
	public boolean getStatus() {
		return sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		sensor.update();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return sensor.getValue();
	}
	
	@Override
	public void setUnitFahrenheit(boolean value) {
		sensor.setUnitFahrenheit(value);
	}
	
	@Override
	public boolean getUnitFahrenheit() {
		return sensor.getUnitFahrenheit();
	}

}
