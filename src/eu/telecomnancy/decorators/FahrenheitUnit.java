package eu.telecomnancy.decorators;

import eu.telecomnancy.proxy.ProxySensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

/* DECORATOR PATTERN : This class changes the unit of the value returned by the sensor from Celsius to Fahrenheit. */

public class FahrenheitUnit extends ValueDecorator {

	public FahrenheitUnit(ProxySensor sensor) {
		super(sensor);
		this.sensor.setUnitFahrenheit(true);
	}
	
	public double getValue() throws SensorNotActivatedException {
		double celsiusValue; 
		celsiusValue = sensor.getValue();
		return (celsiusValue * 1.8 + 32);	
	}
	
}
