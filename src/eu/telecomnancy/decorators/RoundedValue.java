package eu.telecomnancy.decorators;

import java.lang.Math;
import eu.telecomnancy.proxy.ProxySensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

/* DECORATOR PATTERN : This class rounds up the value reuturned by the sensor to the next integer. */

public class RoundedValue extends ValueDecorator {


	public RoundedValue(ProxySensor sensor) {
		super(sensor);
	}
	
	public double getValue() throws SensorNotActivatedException {
		double value; 
		value = sensor.getValue();
		return Math.round(value);	
	}
}
